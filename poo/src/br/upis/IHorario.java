package br.upis;

public interface IHorario {

	//Hora
	void setHora(byte hr);

	byte getHora();

	//Minuto
	void setMinuto(byte min);

	byte getMinuto();

	//Segundo
	void setSegundo(byte seg);

	byte getSegundo();

	String toString();

	//Incrementa Segundo
	void incrementaSegundo();

	//Incrementa Minuto
	void incrementaMinuto();

	//Incrementa Hora
	void incrementaHora();

	//Incrementa V�rios Segundos
	void incrementaVariosSegundos(int Nsegundos);

	//Incrementa V�rios Minutos
	void incrementaVariosMinutos(int Nminutos);

	//Incrementa V�rias Horas
	void incrementaVariasHoras(int Nhoras);

	//Verifica o �ltimo Segundo
	boolean ehUltimoHorario();

	//Verifica o Primeiro Segundo
	boolean ehPrimeiroHorario();

	//Igualdade
	boolean equals(Object obj);

}