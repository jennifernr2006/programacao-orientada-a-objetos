package br.upis;

public class Horario implements IHorario {
	
	private byte hora; 
	private byte minuto; 
	private byte segundo;
	
	public Horario() {
		setHora((byte)0);
		setMinuto((byte)0);
		setSegundo((byte)0);
	}
	
	public Horario(byte hora, byte minuto, byte segundo){
		setHora(hora);
		setMinuto(minuto);
		setSegundo(segundo);
	}
	
	public Horario(int hora, int minuto, int segundo) {
		this((byte)hora, (byte)minuto, (byte)segundo);
	}

	public Horario(IHorario horario) {
		this(horario.getHora(), horario.getMinuto(), horario.getSegundo());
	}
	
	//Hora
		@Override
		public void setHora(byte hr) {
			if(hr >= 0 && hr <= 23) {
				this.hora = hr;
			}		
		}	
		@Override
		public byte getHora() {
			return this.hora;
		}
		
	//Minuto
		@Override
		public void setMinuto(byte min) {
			if (min >=0 && min <= 59) {
				this.minuto = min;
			}
		}
		@Override
		public byte getMinuto() {
			return this.minuto;
		}
		
	//Segundo
		@Override
		public void setSegundo(byte seg) {
			if (seg >= 0 && seg <= 59) {
				this.segundo = seg;
			}
		}
		@Override
		public byte getSegundo() {
			return this.segundo;
		}	
		
		@Override
		public String toString() {
			return getHora() + ":" + getMinuto() + ":" + getSegundo();
		}	
	//Incrementa Segundo
	@Override
	public void incrementaSegundo() {
		byte s = (byte)(segundo + 1);			
			if(s == 60) {
				segundo = 0;
				incrementaMinuto();
			}else {
				segundo = s;
			}
		}
	//Incrementa Minuto
	@Override
	public void incrementaMinuto() {	
		byte m = (byte)(minuto + 1);	
		if(m == 60) {
			minuto = 0;
			incrementaHora();
		}else {
			minuto = m;
		}
	}
	//Incrementa Hora
	@Override
	public void incrementaHora() {
		byte h = (byte)(hora + 1);
			if(h == 24) {
				hora = 0;
			}else {
				hora = h;
			}			
	}
	//Incrementa V�rios Segundos
	@Override
	public void incrementaVariosSegundos(int Nsegundos) {
		for(int i = 0; i < Nsegundos; i++) {
			incrementaSegundo();
		}
	}
	//Incrementa V�rios Minutos
	@Override
	public void incrementaVariosMinutos(int Nminutos) {
		for(int i = 0; i < Nminutos; i++) {
			incrementaMinuto();
		}
	}
	//Incrementa V�rias Horas
	@Override
	public void incrementaVariasHoras(int Nhoras) {
		for(int i = 0; i < Nhoras; i++) {
			incrementaHora();
		}
	}
	//Verifica o �ltimo Segundo
	@Override
	public boolean ehUltimoHorario() {
		return hora == 23 && minuto == 59 && segundo == 59;
	}
	//Verifica o Primeiro Segundo
	@Override
	public boolean ehPrimeiroHorario() {
		return hora == 0 && minuto == 0 && segundo == 0;
	}
	//Igualdade
	@Override
	public boolean equals(Object obj) {
		if(this == obj) {
			return true;
		}
		if(obj == null || obj.getClass()!=this.getClass()) {
			return false;
		}
		IHorario h = (IHorario) obj;
			return this.getHora() == h.getHora() && 
					this.getMinuto() == h.getMinuto() && 
					this.getSegundo() == h.getSegundo();
		}			
}
