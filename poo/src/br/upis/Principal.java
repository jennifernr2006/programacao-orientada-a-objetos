package br.upis;

public class Principal {
	public static void main(String[] args) {
		//Comparando Horario		
		IHorario h1 = new Horario(10, 25, 30);
		IHorario h2 = new Horario(10, 25, 30);
		
		if (h2.equals(h1)) {
			System.out.println(h1 + " � igual a " + h2);
			System.out.println("Iguais");
		}else {
			System.out.println(h1 + " � diferente de " + h2);
			System.out.println("Diferentes");
		}
		
		//Comparando Data
		IData d1 = new Data(13, 12, 2021);
		IData d2 = new Data(10, 12, 2021);
		
		if (d2.equals(d1)) {
			System.out.println(d1 + " � igual a " + d2);
			System.out.println("Iguais");
		}else {
			System.out.println(d1 + " � diferente de " + d2);
			System.out.println("Diferentes");
		}
	}
}
