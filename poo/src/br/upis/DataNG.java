package br.upis;

public class DataNG implements IData {

	private byte dia;
	
	private boolean ehBissexto(int dia) {
		return (dia % 146100 == 0 || ((dia % 36525 == 0) && (dia % 1461 != 0)));
	}
	
	private byte getUltimoDia(int dia) {
		byte ud [] = {0,31,28,31,30,31,30,31,31,30,31,30,31};
		int mes = procuraMes(dia);
		if(mes == 2 && ehBissexto(dia)) {
			return 29;
		}
		return ud[mes];
	}
	//Mostrar Dia
	@Override
	public byte procuraDia(int dia) {
		byte ud [] = {0,31,28,31,30,31,30,31,31,30,31,30,31};
		int mes;
		for(mes = 0; dia > ud[mes]; dia++) {
			dia = dia - ud[mes];
		}
		return (byte)dia;
	}
	//Mostrar M�s
	@Override
	public byte procuraMes(int dia) {
		byte ud [] = {0,31,28,31,30,31,30,31,31,30,31,30,31};
		int mes;
		for(mes = 0; dia > ud[mes]; dia++) {
			dia = dia - ud[mes];
		}
		return (byte)mes;
	}
	//----------
	public DataNG() {
		setDia((byte)1);
	}
	public DataNG(byte dia, byte mes, short ano) {
		this();
		setDia(dia);
	}
	public DataNG(int dia, int mes, int ano) {
		this((byte)dia, (byte)mes, (short)ano);
	}

	//Dia
	@Override
	public byte getDia() {
		return procuraDia(dia);
	}
	@Override
	public void setDia(byte dia) {
		int ultimoDia = getUltimoDia(dia);
		if(dia >= 1 && dia <= ultimoDia) {
			this.dia = dia;
		}
	}
	//M�s
	@Override
	public byte getMes() {
		return (byte)(procuraMes(dia));
	}		
	@Override
	public void setMes(byte dia) {
		int mes = procuraMes(dia);
		if(mes >= 1 && mes <= 12) {
			dia = (byte) mes;
		}
	}
	//Ano
	@Override
	public short getAno() {
		return (short)(dia % 365);
	}
	@Override
	public void setAno(short dia) {
		int ano = 0;
		if(procuraMes(dia) > 12) {
			ano++;
		}
		if(ano >=1 && ano <= 9999) {
			dia = (short) ano;
		}
	}
	//Incrementa Dia
	@Override
	public void incrementaDia() {
		byte d = (byte)(dia + 1);		
		if(d == getUltimoDia(dia)) {
			dia = 1;
			incrementaMes();
		}else {
			setDia(dia);
		}
	}
	//Incrementa M�s
	@Override
	public void incrementaMes() {
	byte mes = (byte)(procuraMes(dia));		
		if(mes == 12) {
			mes = 1;
			incrementaAno();
		}else {
			setMes(mes);
		}
	}
	//Incrementa Ano
	@Override
	public void incrementaAno() {
		short ano = 0;
		if(procuraMes(dia) > 12) {
			ano++;
		}
		if(ano == 9999) {
			ano = 1;
		}else {
			setAno(ano);
		}
	}
	//Incrementa V�rios Dias
	@Override
	public void incrementaVariosDias(int Ndias) {
		for(int i = 0; i < Ndias; i++) {
			incrementaDia();
		}
	}
	//Incrementa V�rios Meses
	@Override
	public void incrementaVariosMeses(int Nmeses) {
		for(int i = 0; i < Nmeses; i++) {
			incrementaMes();
		}
	}
	//Incrementa V�rios Anos
	@Override
	public void incrementaVariosAnos(int Nanos) {
		for(int i = 0; i < Nanos; i++) {
			incrementaAno();
		}
	}
	//Igualdade
	@Override
	public boolean equals(Object obj) {
		if(this == obj) {
			return true;
		}
		if(obj == null || obj.getClass() != this.getClass()) {
			return false;
		}
		IData d = (IData) obj;
		return this.getAno() == d.getAno() && 
				this.getMes() == d.getMes() && 
				this.getDia() == d.getDia();
	}
}

