package br.upis;

public class Data implements IData {
	private byte dia;
	private byte mes;
	private short ano;
	
	private boolean ehBissexto(int ano) {
		return (ano % 400 == 0 || ((ano % 4 == 0) && (ano % 100 != 0)));
	}
	
	private byte getUltimoDia(int mes, int ano) {
		byte ud [] = {0,31,28,31,30,31,30,31,31,30,31,30,31};
		
		if(mes == 2 && ehBissexto(ano)) {
			return 29;
		}
		return ud[mes];
	}
	//Mostrar Dia
		@Override
		public byte procuraDia(int dia) {
			byte ud [] = {0,31,28,31,30,31,30,31,31,30,31,30,31};
			int mes;
			for(mes = 0; dia > ud[mes]; dia++) {
				dia = dia - ud[mes];
			}
			return (byte)dia;
		}
	//Mostrar M�s
		@Override
		public byte procuraMes(int dia) {
			byte ud [] = {0,31,28,31,30,31,30,31,31,30,31,30,31};
			int mes;
			for(mes = 0; dia > ud[mes]; dia++) {
				dia = dia - ud[mes];
			}
			return (byte)mes;
		}
	public Data() {
		setAno((byte)1);
		setMes((byte)1);
		setDia((byte)1);
	}
	public Data(byte dia, byte mes, short ano) {
		this();
		setAno(ano);
		setMes(mes);
		setDia(dia);
	}
	public Data(int dia, int mes, int ano) {
		this((byte)dia, (byte)mes, (short)ano);
	}
	//Dia
	@Override
	public byte getDia() {
		return dia;
	}
	@Override
	public void setDia(byte dia) {
		int ultimoDia = getUltimoDia(mes, ano);
		if(dia >= 1 && dia <= ultimoDia) {
			this.dia = dia;
		}
	}
	//M�s
	@Override
	public byte getMes() {
		return mes;
	}
	
	@Override
	public void setMes(byte mes) {
		if(mes >= 1 && mes <= 12) {
			this.mes = mes;
		}
	}
	//Ano
	@Override
	public short getAno() {
		return ano;
	}
	@Override
	public void setAno(short ano) {
		if(ano >=1 && ano <= 9999) {
			this.ano = ano;
		}
	}
	//Incrementa Dia
	@Override
	public void incrementaDia() {
		byte d = (byte)(dia + 1);
		
		if(d == getUltimoDia(mes, ano)) {
			dia = 1;
			incrementaMes();
		}else {
			setDia(dia);
		}
	}
	//Incrementa M�s
	@Override
	public void incrementaMes() {
		byte m = (byte)(mes + 1);
		
		if(m == 12) {
			mes = 1;
			incrementaAno();
		}else {
			setMes(mes);
		}
	}
	//Incrementa Ano
	@Override
	public void incrementaAno() {
		short a = (short)(ano + 1);
		if(a == 9999) {
			ano = 1;
		}else {
			setAno(ano);
		}
	}
	//Incrementa V�rios Dias
	@Override
	public void incrementaVariosDias(int Ndias) {
		for(int i = 0; i < Ndias; i++) {
			incrementaDia();
		}
	}
	//Incrementa V�rios Meses
	@Override
	public void incrementaVariosMeses(int Nmeses) {
		for(int i = 0; i < Nmeses; i++) {
			incrementaMes();
		}
	}
	//Incrementa V�rios Anos
	@Override
	public void incrementaVariosAnos(int Nanos) {
		for(int i = 0; i < Nanos; i++) {
			incrementaAno();
		}
	}
	//Igualdade
		@Override
		public boolean equals(Object obj) {
			if(this == obj) {
				return true;
			}
			if(obj == null || obj.getClass() != this.getClass()) {
				return false;
			}
			IData d = (IData) obj;
			return this.getAno() == d.getAno() && 
					this.getMes() == d.getMes() && 
					this.getDia() == d.getDia();
		}

}

