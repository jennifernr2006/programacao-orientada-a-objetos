package br.upis;

public class HorarioNG implements IHorario {

	private byte segundo;
		
		public void Horario () {
			setSegundo((byte)0);
		}
		
		public void Horario (byte segundo) {
			setSegundo(segundo);
		}
		
		//Hora
			public void setHora(byte hr) {
				if (hr >= 0 && hr <= 23) {
					segundo = (byte)((segundo / 3600) % 24);
				}		
			}	
			public byte getHora() {
				return (byte)((segundo / 3600)% 24);
			}
			
		//Minuto
			public void setMinuto(byte min) {
				if (min >=0 && min <= 59) {
					segundo = (byte)((segundo / 60) % 60);
				}
			}
			public byte getMinuto() {
				return (byte)((segundo / 60) % 60);
			}
			
		//Segundo
			public void setSegundo(byte seg) {
				if (seg >= 0 && seg <= 59) {
					this.segundo = seg;
				}
			}
			public byte getSegundo() {
				return (byte)(segundo % 60);
			}	
			
			public String toString() {
				return getHora() + ":" + getMinuto() + ":" + getSegundo();
			}	
			//Incrementa Segundo
			public void incrementaSegundo() {
				if(segundo < 86400) {
					segundo = (byte) (segundo + 1);
				}else {
					incrementaMinuto();
				}
			}
			//Incrementa Minuto
			public void incrementaMinuto() {	
				if(segundo < 1440) {
					segundo = (byte)(segundo + 60);
				}else {
					incrementaHora();
				}
			}
			//Incrementa Hora
			public void incrementaHora() {
				if(segundo < 24) {
					segundo = (byte)(segundo + 3600);
				}			
			}

	//Incrementa V�rios Segundos
	@Override
	public void incrementaVariosSegundos(int Nsegundos) {
		for (int i = 0; i < Nsegundos; i++) {
			incrementaSegundo();
		}
	}
	//Incrementa V�rios Minutos
	@Override
	public void incrementaVariosMinutos(int Nminutos) {
		for (int i = 0; i < Nminutos; i++) {
			incrementaMinuto();
		}
	}
	//Incrementa V�rias Horas
	@Override
	public void incrementaVariasHoras(int Nhoras) {
		for (int i = 0; i < Nhoras; i++) {
			incrementaHora();
		}
	}
	//Verifica o �ltimo Segundo
	@Override
	public boolean ehUltimoHorario() {
		return segundo == 86399;
	}
	//Verifica o Primeiro Segundo
	@Override
	public boolean ehPrimeiroHorario() {
		return segundo == 0;
	}
	//Igualdade
	@Override
	public boolean equals(Object obj) {
		if(this == obj) {
			return true;
		}
		if(obj == null || obj.getClass()!=this.getClass()) {
			return false;
		}
		IHorario h = (IHorario) obj;
			return this.getHora() == h.getHora() && 
					this.getMinuto() == h.getMinuto() && 
					this.getSegundo() == h.getSegundo();
		}			
}