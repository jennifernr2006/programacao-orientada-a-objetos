
package br.upis;

public class Relogio {

	private IHorario hms;
	private IData dma;
	
	public Relogio(IHorario hms, IData dma) {
		this.hms = new Horario(hms);
		this.dma = dma;
	}
	
	public void tictac() {
		
		hms.incrementaSegundo();

		if(hms.ehPrimeiroHorario()) {
			dma.incrementaDia();
		}		
	}
	
	@Override
	public String toString() {
		return dma + " " + hms;
	}
	public boolean equals(Object obj) {
		if(this == obj) {
			return true;
		}
		if(obj == null || obj.getClass() != this.getClass()) {
			return false;
		}
		Relogio r = (Relogio) obj;
		return this.dma.getAno() == r.dma.getAno() &&
				this.dma.getMes() == r.dma.getMes() &&
				this.dma.getDia() == r.dma.getDia() &&
				this.hms.getHora() == r.hms.getHora() &&
				this.hms.getMinuto() == r.hms.getMinuto() &&
				this.hms.getSegundo() == r.hms.getSegundo();
	}
}

