package br.upis;

public interface IData {

	//Mostrar Dia
	byte procuraDia(int dia);

	//Mostrar M�s
	byte procuraMes(int dia);

	//Dia
	byte getDia();

	void setDia(byte dia);

	//M�s
	byte getMes();

	void setMes(byte mes);

	//Ano
	short getAno();

	void setAno(short ano);

	//Incrementa Dia
	void incrementaDia();

	//Incrementa M�s
	void incrementaMes();

	//Incrementa Ano
	void incrementaAno();

	//Incrementa V�rios Dias
	void incrementaVariosDias(int Ndias);

	//Incrementa V�rios Meses
	void incrementaVariosMeses(int Nmeses);

	//Incrementa V�rios Anos
	void incrementaVariosAnos(int Nanos);

	//Igualdade
	boolean equals(Object obj);

}